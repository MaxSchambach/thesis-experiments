"""
CS Vector Dictionary evaluation.

Evaluates the approach using the dataset challenges and the real-world data.
"""

from lfcnn.utils.tf_utils import use_cpu
use_cpu()
from config.base_config import get_experiment
from config.base_config import get_trained_model
from config.base_config import evaluate, predict

# Set experiment, load default ingredients
ex = get_experiment("CsVectorDictEvaluate")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "network.cs_vector_dict_epinet_eval",
                 "trained_model.cs_vector_dict_epinet",  # Update ID of previously trained dictionary
                 "network.loss_cs_vector_dict_epinet"]

CONFIG_UPDATES = {"dataset.copy": False,
                  "settings.use_mask": True,
                  "settings.epochs": 1,
                  "settings.batch_size": 1}


@ex.capture
def run_experiment(settings, network, trained_model, _run, _log):
    model = get_trained_model(trained_model, network, predict=False)
    evaluate(model, "challenges", settings, network, _run, _log)
    evaluate(model, "wall", settings, network, _run, _log)
    predict(model, "real_own", settings, network, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
