"""
CS 5D-DCT experiment.
"""
from datetime import datetime

from config.base_config import get_experiment
from config.cs_config import reconstruct

# Set experiment, load default ingredients
ex = get_experiment("Cs5dDct")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral"]

CONFIG_UPDATES = {"dataset.copy": False,
                  "settings.penalty_coeff": 0.01}


@ex.capture
def run_experiment(settings, dataset, _run, _log):
    # You may want to run these reconstructions individually or in parallel
    # as they take a lot of time...
    reconstruct(settings, dataset, _run, _log, which="test_dataset")
    reconstruct(settings, dataset, _run, _log, which="challenges")
    reconstruct(settings, dataset, _run, _log, which="wall")
    reconstruct(settings, dataset, _run, _log, which="real_own")


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
