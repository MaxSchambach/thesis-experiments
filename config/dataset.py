"""Dataset definitions."""
from pathlib import Path
import socket
from typing import Optional

from sacred import Ingredient

# Get current host
HOSTNAME = socket.gethostname()

# Define DATASET Ingredient and configurations
dataset = Ingredient("dataset")


@dataset.config
def default():
    copy = True
    dataset = None
    range_data = None
    data_path: Optional[Path] = None
    challenge_path: Optional[Path] = None
    challenge_wall_path: Optional[Path] = None
    predict_path: Optional[Path] = None
    train_path: Optional[Path] = None if data_path is None else data_path / "train.h5"
    valid_path: Optional[Path] = None if data_path is None else data_path / "validation.h5"
    test_path: Optional[Path] = None if data_path is None else data_path / "test.h5"

    if copy:
        import os
        import subprocess

        try:
            trgt = Path(os.environ['TMP'])
        except KeyError:
            raise ValueError("Need to set $TMP environment variable for copy target.")

        print("Copying data to $TMP...")
        if train_path is not None:
            print(f"Copying {train_path.name}")
            train_path_trgt = trgt / train_path.name
            args = ["cp", str(train_path), str(train_path_trgt)]
            subprocess.run(args)
            train_path = train_path_trgt

        if valid_path is not None:
            print(f"Copying {valid_path.name}")
            valid_path_trgt = trgt / valid_path.name
            args = ["cp", str(valid_path), str(valid_path_trgt)]
            subprocess.run(args)
            valid_path = valid_path_trgt

        if test_path is not None:
            print(f"Copying {test_path.name}")
            test_path_trgt = trgt / test_path.name
            args = ["cp", str(test_path), str(test_path_trgt)]
            subprocess.run(args)
            test_path = test_path_trgt

        if challenge_path is not None:
            print(f"Copying {challenge_path.name}")
            challenge_path_trgt = trgt / challenge_path.name
            args = ["cp", str(challenge_path), str(challenge_path_trgt)]
            subprocess.run(args)
            challenge_path = challenge_path_trgt

        if challenge_wall_path is not None:
            print(f"Copying {challenge_wall_path.name}")
            challenge_wall_path_trgt = trgt / challenge_wall_path.name
            args = ["cp", str(challenge_wall_path), str(challenge_wall_path_trgt)]
            subprocess.run(args)
            challenge_wall_path = challenge_wall_path_trgt

        if predict_path is not None:
            print(f"Copying {predict_path.name}")
            predict_path_trgt = trgt / predict_path.name
            args = ["cp", str(predict_path), str(predict_path_trgt)]
            subprocess.run(args)
            predict_path = predict_path_trgt

        print("... done.")


@dataset.named_config
def multispectral():
    """Use the LFCNN multispectral Dataset at 9x9 angular resolution as default
    """
    dataset = "MultiSpectralDataset"
    range_data = 2 ** 16 - 1  # 16 Bit data
    num_data = 78400

    # Set dataset locations
    base_path = Path("<path-to-dataset>/MULTISPECTRAL")
    data_path = base_path / "DATA/PATCHED_F_9x9_36x36"
    challenge_path = base_path / "CHALLENGES/COMPOSED/9x9_CHALLENGE_MS_LF_DISP_CENTRAL.h5"
    challenge_wall_path = base_path / "CHALLENGES/COMPOSED/9x9_WALL_MS_LF_DISP_CENTRAL.h5"
    real_own_path = base_path / "../REAL/MULTISPECTRAL/9x9_REAL_FOCUSED.h5"
    real_xiong_path = base_path / "../../downsampled/9x9_REAL_MS_XIONG.h5"


@dataset.named_config
def rgb():
    """Use the LFCNN RGB Dataset at 9x9 angular resolution
    """
    dataset = "RgbDataset"
    range_data = 2 ** 16 - 1  # 16 Bit data
    num_data = 78400

    # Set dataset locations
    base_path = Path("<path-to-dataset>/RGB")
    data_path = base_path / "DATA/PATCHED_F_9x9_36x36"
    challenge_path = base_path / "CHALLENGES/COMPOSED/9x9_CHALLENGE_RGB_LF_DISP_CENTRAL.h5"
    challenge_wall_path = base_path / "CHALLENGES/COMPOSED/9x9_WALL_RGB_LF_DISP_CENTRAL.h5"
    real_own_path = base_path / "../REAL/RGB/9x9_REAL_RGB_FOCUSED.h5"
    real_xiong_path = base_path / "../../downsampled/9x9_REAL_RGB_XIONG.h5"
