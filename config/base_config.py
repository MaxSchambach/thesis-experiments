"""Base configuration of all experiments."""
from pathlib import Path
import platform
import socket
from typing import List
import os

import mdbh

# Optional: Set multithreading
import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(0)
tf.config.threading.set_intra_op_parallelism_threads(0)
from tensorflow.keras import optimizers
from tensorflow.keras.callbacks import ReduceLROnPlateau

import matplotlib.pyplot as plt
import numpy as np
from plenpy.spectral import SpectralImage

from lfcnn import losses
from lfcnn.callbacks import ExponentialDecay
from lfcnn.callbacks import LinearDecay
from lfcnn.callbacks import LearningRateFinder
from lfcnn.callbacks import OneCycle
from lfcnn.callbacks import OneCycleMomentum
from lfcnn.callbacks import SacredEpochLogger
from lfcnn.callbacks import SacredLearningRateLogger
from lfcnn.callbacks import SacredLossWeightLogger
from lfcnn.callbacks import SacredMetricLogger
from lfcnn.callbacks import SacredTimeLogger
from lfcnn.callbacks import SigmoidDecay
from lfcnn.callbacks import StepListDecay
from lfcnn.metrics import get_central_metrics_fullsize
from lfcnn.metrics import get_central_metrics_small
from lfcnn.metrics import get_disparity_metrics
from lfcnn.metrics import get_lf_metrics
from lfcnn.models import get_model_type
from lfcnn.models import BaseModel
from lfcnn.layers.masks.callbacks import MaskEntryopyLossScheduler
from lfcnn.layers.masks.callbacks import MaskEntryopyRelativeLossScheduler
from lfcnn.layers.masks.callbacks import MaskSaveCallback
from lfcnn.layers.masks.callbacks import MaskTemperatureScheduler
from lfcnn.training import get_model_cls

from sacred import Experiment
from sacred import SETTINGS
from sacred.observers import MongoObserver

from .dataset import dataset
from .trained_model import trained_model
from .network import network
from .settings import settings

# Get current host
HOSTNAME = socket.gethostname()

# Set .mongo conf file (see MDBH library)
MONGO_CONF = "~/.mongo.conf"

# Set persistent cache folder for downloaded artifacts
cache_dir = Path(os.environ['HOME']) / ".mdbh" / "cache"
mdbh.environ.set_cache_dir(str(cache_dir.resolve()))

# On some installations, you may need to define a TMP folder manually
# os.environ['TMP'] = "/tmp"

# Change sacred capture mode to workaround multiprocessing bug
# https://github.com/IDSIA/sacred/issues/705
SETTINGS['CAPTURE_MODE'] = 'sys'

# For Windows, disable read only list as it leads to error with multiprocessing
# https://github.com/IDSIA/sacred/issues/509
if platform.system().lower() == "windows":
    SETTINGS.CONFIG.READ_ONLY_CONFIG = False


############################
# Define Experiment Getter #
############################
def get_experiment(name: str,
                   db_name: str = "sacred",
                   mongo_conf: str = MONGO_CONF,
                   disable_capture: bool = True,
                   observe: bool = True) -> Experiment:
    """Get an experiment with given name and set a MongoObserver with
    automated configuration and authentication.

    Captured output logging is deactivated by default, as it results in a lot of
    overhead together with the MongoObserver, sometimes even in timeouts.

    Args:
        name: Name of the experiment.

        db_name: Name of the MongoDB database.

        mongo_conf: Name of the MongoDB config file used by mdbh.

        disable_capture: Whether to disable captured output logging to MongoDB.

        observer: Whether to add an Observer or not. Useful when debugging.

    Returns:
        Sacred experiment with MongoOberserver.
    """

    # Init experiment with Ingredients
    ingredients = [settings, network, dataset, trained_model]
    ex = Experiment(name, ingredients=ingredients)

    if observe:
        # Set Sacred MongoDB database name
        uri = mdbh.get_uri(mongo_conf, db_name)
        ex.observers.append(MongoObserver(url=uri, db_name=db_name))

    if disable_capture:
        # Disable output capturing explicitly as it may lead to hangups with MongoDB
        ex.captured_out_filter = lambda text: 'Output capturing turned off.'

    return ex


###################################################
# Define Network Training, Testing and Evaluation #
###################################################
@network.capture
def train_and_test(settings, network, _run, _log):
    """Train and test model.

    Returns:
        Trained LFCNN moodel instance.
    """

    # Get model class
    model = get_model_type(network["model_type"]).get(network["model"])
    model_cls = get_model_cls(network["model_cls"])
    model_cls_kwargs = network["model_cls_kwargs"]

    # Load and init losses
    loss, loss_weights = get_loss_and_weights(network)

    # Load and init optimizer
    optimizer = get_optimizer(network)

    # Init metrics
    if network["model_type"].lower() == "sparse_coding":
        metrics = dict(light_field=get_lf_metrics())

    elif network["model_type"].lower() == "disparity":
        metrics = dict(disparity=get_disparity_metrics())

    elif network["model_type"].lower() == "center_and_disparity":
        if network['model'].lower() == "conv3ddecode2dstcentral":
            metrics = dict(central_view=get_central_metrics_small())
        elif network['model'].lower() == "conv3ddecode2dstdisp":
            metrics = dict(disparity=get_disparity_metrics())
        else:
            metrics = dict(disparity=get_disparity_metrics(),
                           central_view=get_central_metrics_small())

    else:
        raise ValueError(f"No metrics specified for model type {network['model_type']}.")
    
    # Set up lr schedule
    if network["schedule"] is None:
        callbacks = []

    elif network["schedule"].lower() == "1cycle":
        # Setup Cyclic learning
        cycle_epoch = int(0.7*settings['epochs'])

        lr_sched = OneCycle(lr_min=network['lr_min'],
                            lr_max=network['lr_max'],
                            lr_final=network['lr_final'],
                            cycle_epoch=cycle_epoch,
                            max_epoch=settings['epochs'])
        mom_schel = OneCycleMomentum(cycle_epoch=cycle_epoch, m_min=0.85, m_max=0.95)
        callbacks = [lr_sched, mom_schel]

    elif network["schedule"].lower() == "reducelronplateau":
        lr_sched = ReduceLROnPlateau(monitor='val_loss', factor=0.25, patience=8, mode='min')
        callbacks = [lr_sched]

    elif network["schedule"].lower() == "steplistdecay":
        lr_sched = StepListDecay(lr_init=network['lr'],
                                 steps=[int(i*settings['epochs']) for i in [0.75, 0.85, 0.9, 0.95, 0.975]],
                                 decay=0.5)
        callbacks = [lr_sched]

    elif network["schedule"].lower() == "linear":
        lr_sched = LinearDecay(lr_init=network['lr'],
                               max_epoch=settings['epochs'] - 1,
                               lr_min=2e-2*network['lr'])
        callbacks = [lr_sched]

    elif network["schedule"].lower() == "exponential":
        lr_sched = ExponentialDecay(lr_init=network['lr'],
                                    max_epoch=settings['epochs'] - 1,
                                    alpha=0.025,
                                    lr_min=2e-2*network['lr'])
        callbacks = [lr_sched]

    elif network["schedule"].lower() == "sigmoid":
        lr_sched = SigmoidDecay(lr_init=network['lr'],
                                max_epoch=settings['epochs'] - 1,
                                alpha=0.1,
                                offset=int(0.6*settings['epochs']),
                                lr_min=2e-2*network['lr'])
        callbacks = [lr_sched]

    else:
        print("No valid LR schedule specified. Continuing without schedule.")
        callbacks = []

    # Set up Sacred callbacks
    callbacks += get_sacred_callbacks(_run, epochs=settings['epochs'], offset=0)

    # Add mask entropy regularizer callback
    if network['model'] == "conv3ddecode2dmasked":

        # Add mask entropy regularizer callback
        if network['lambda_e_init'] is not None and not network['mask_type'] == "dummy":

            callbacks += [MaskEntryopyLossScheduler(network['lambda_e_init'],
                                                    network['lambda_e_end'],
                                                    settings['epochs'])]

        # Add mask temperature callback
        if network['inv_temp_init'] is not None and not network['mask_type'] == "dummy":
            callbacks += [MaskTemperatureScheduler(network['inv_temp_init'],
                                                   network['inv_temp_end'],
                                                   settings['epochs'])]

        # Calculate batch skip to obtain a 15s video
        num_batches = settings['data_percentage'] * 78400 / settings['batch_size']
        num_epochs = settings['epochs']
        fps = 4
        duration = 15
        batchskip = max(1, int(num_batches * num_epochs / fps / duration))
        print(f"Saving mask history every {batchskip} batches.")
        callbacks += [MaskSaveCallback(_run, batchskip=batchskip, filename="mask_history_train")]

    # Init and compile model
    model = model(**network['model_kwargs'],
                  optimizer=optimizer, loss=loss, metrics=metrics,
                  callbacks=callbacks, loss_weights=loss_weights,
                  model_cls=model_cls, model_cls_kwargs=model_cls_kwargs)

    # Train and validate
    print("Training...")
    hist = model.train(**settings['train_kwargs'])
    print("... done.")

    # Test
    print("Testing...")
    test_res = model.test(**settings['test_kwargs'])
    print("... done.")

    # Log test results to sacred
    print("Logging test results...")
    for key in test_res:
        _run.log_scalar("test_" + key, test_res[key], 0)
    print("... done.")

    if settings["save_weights"]:
        print("Saving weights...")
        # Save to $TMP
        tmp_dir = Path(os.environ['TMP'])
        weights_path = str(tmp_dir / "weights.h5")
        model.save_weights(weights_path)
        _run.add_artifact(weights_path)
        print("... done.")

    return model


@network.capture
def train_mask_optimization(model, settings, network, dataset, _run, _log):
    """Train and test a pretrained model with mask optimization.

    Returns:
        Trained LFCNN moodel instance.
    """

    # Set up lr schedule for warmup, main training and finetuning
    epochs_warmup = settings['epochs_warmup']
    epochs_main = settings['epochs']
    epochs_finetune = settings['epochs_finetune']
    epochs_total = epochs_warmup + epochs_main + epochs_finetune

    # Set metrics
    metrics = dict(disparity=get_disparity_metrics(),
                   central_view=get_central_metrics_small())
    model.set_metrics(metrics)

    # Setup custom training strategy using two optimizers,
    # one for the CNN, one for the mask
    model.set_model_cls(get_model_cls("MaskOptimization"))
    model.set_model_cls_kwargs(dict(mask_lr=1e-3))

    train_kwargs = settings['train_kwargs'].copy()
    if not network['mask_type'] == "dummy" and settings['epochs_warmup'] > 0:
        ########
        # WARMUP
        ########
        # Setup callbacks
        sacred_callbacks_warmup = get_sacred_callbacks(_run, epochs=epochs_total)
        lr_sched_warmup = SigmoidDecay(lr_init=network['lr'],
                                       max_epoch=epochs_warmup - 1,
                                       alpha=1.0,
                                       lr_min=0.5*network['lr'])
        # Calculate batch skip to obtain a 15s video
        num_batches = settings['data_percentage']*dataset['num_data']/settings['batch_size']
        num_epochs = epochs_warmup
        fps = 4
        duration = 15
        batchskip = max(1, int(num_batches*num_epochs/fps/duration))
        print(f"Saving mask history every {batchskip} batches.")
        mask_save_cb = MaskSaveCallback(_run, batchskip=batchskip, filename="mask_history_warmup")

        callbacks = [lr_sched_warmup] + sacred_callbacks_warmup + [mask_save_cb]
        model.set_callbacks(callbacks)

        print(f"Warmup {epochs_warmup} epochs...")
        train_kwargs['epochs'] = epochs_warmup
        model.set_warmup(True)
        hist = model.train(**train_kwargs)
        model.set_warmup(False)
        print("... done.")


    ########
    # MAIN TRAINING
    ########
    if settings['epochs'] > 0:
        # Reset metrics
        metrics = dict(disparity=get_disparity_metrics(),
                       central_view=get_central_metrics_small())
        model.set_metrics(metrics)

        mask_schedule_callbacks = []
        # Add mask entropy regularizer callback
        if network['lambda_e_init'] is not None and not network['mask_type'] == "dummy":
            lambda_e_init = network['mask_kwargs']['lambda_e']
            lambda_e_end = 1e3 * lambda_e_init
            mask_entropy_callback = [MaskEntryopyLossScheduler(lambda_e_init, lambda_e_end, epochs_main)]

            # lambda_e_perc_init = network['lambda_e_init']
            # lambda_e_perc_end = network['lambda_e_end']
            # mask_schedule_callbacks += [MaskEntryopyRelativeLossScheduler(lambda_e_perc_init, lambda_e_perc_end, epochs_main, offset=5)]

        # Add mask temperature callback
        if network['inv_temp_init'] is not None and not network['mask_type'] == "dummy":
            mask_schedule_callbacks += [MaskTemperatureScheduler(network['inv_temp_init'], network['inv_temp_end'], epochs_main)]

        lr_sched_train = SigmoidDecay(lr_init=network['lr'],
                                      max_epoch=epochs_main - 1,
                                      alpha=0.5,
                                      lr_min=1e-1*network['lr'])
        sacred_callbacks_train = get_sacred_callbacks(_run,
                                                      epochs=epochs_total,
                                                      offset=epochs_warmup)

        # Calculate batch skip to obtain a 15s video
        num_batches = settings['data_percentage']*dataset['num_data'] / settings['batch_size']
        num_epochs = epochs_main
        fps = 4
        duration = 15
        batchskip = max(1, int(num_batches * num_epochs / fps / duration))
        print(f"Saving mask history every {batchskip} batches.")

        mask_save_cb = MaskSaveCallback(_run, batchskip=batchskip, filename="mask_history_train")
        callbacks = [lr_sched_train] + sacred_callbacks_train + mask_schedule_callbacks + [mask_save_cb]
        model.set_callbacks(callbacks)

        print(f"Training {epochs_main} epochs...")
        hist = model.train(**settings['train_kwargs'])
        print("... done.")

    ########
    # FINETUNE
    ########
    if settings['epochs_finetune'] > 0:
        # Reset metrics
        metrics = dict(disparity=get_disparity_metrics(),
                       central_view=get_central_metrics_small())
        model.set_metrics(metrics)

        # Set callbacks
        lr_sched_finetune = SigmoidDecay(lr_init=1e-1*network['lr'],
                                         max_epoch=epochs_finetune - 1,
                                         alpha=1.0,
                                         lr_min=1e-2*network['lr'])
        sacred_callbacks_finetune = get_sacred_callbacks(_run,
                                                         epochs=epochs_total,
                                                         offset=epochs_warmup+epochs_main)
        callbacks = [lr_sched_finetune] + sacred_callbacks_finetune
        model.set_callbacks(callbacks)

        # Enable GradNorm with adaptive multi task training
        model.set_model_cls(get_model_cls("NormalizedGradientSimilarity"))
        model.set_model_cls_kwargs(dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"],
                                                        central_view=["NormalizedStructuralSimilarity",
                                                                      "NormalizedCosineProximity"]),
                                        gradient_approximation="None",
                                        multi_task_uncertainty=True,
                                        weights_init=dict(disparity=10.0, central_view=100.0)))

        print(f"Finetuning {epochs_finetune} epochs...")
        model.set_finetune(True)
        train_kwargs['epochs'] = epochs_finetune
        hist = model.train(**train_kwargs)
        print("... done.")

    ########
    # TEST
    ########
    print("Testing...")
    test_res = model.test(**settings['test_kwargs'])
    print("... done.")

    # Log test results to sacred
    print("Logging test results...")
    for key in test_res:
        _run.log_scalar("test_" + key, test_res[key], 0)
    print("... done.")

    if settings["save_weights"]:
        print("Saving weights...")
        # Save to $TMP
        tmp_dir = Path(os.environ['TMP'])
        weights_path = str(tmp_dir/"weights.h5")
        model.save_weights(weights_path)
        _run.add_artifact(weights_path)
        print("... done.")

    return model


@network.capture
def test(model: BaseModel,
         settings, network, _run, _log):
    """Test a trained model.

    Args:
        model: Trained LFCNN model instance.
    """
    # Init metrics
    if network["model_type"].lower() == "sparse_coding":
        metrics = dict(light_field=get_lf_metrics())

    elif network["model_type"].lower() == "disparity":
        metrics = dict(disparity=get_disparity_metrics())

    elif network["model_type"].lower() == "center_and_disparity":
        # Update metrics to evaluate full sized MS-SSIM if large enough data
        if settings['test_kwargs']["augmented_shape"][2] >= 128 and \
                settings['test_kwargs']["augmented_shape"][3] >= 128:
            metrics = dict(disparity=get_disparity_metrics(),
                           central_view=get_central_metrics_fullsize())
        else:
            metrics = dict(disparity=get_disparity_metrics(),
                           central_view=get_central_metrics_small())
    else:
        raise ValueError(f"No metrics specified for model type {network['model_type']}.")

    model.set_metrics(metrics)

    # Test
    print("Testing...")
    test_res = model.test(**settings['test_kwargs'])
    print("... done.")

    # Log test results to sacred
    print("Logging test results...")
    for key in test_res:
        _run.log_scalar("test_" + key, test_res[key], 0)
    print("... done.")

    return


@network.capture
def test_noisy(model: BaseModel,
               settings, network, _run, _log,
               psnr_values=None):
    """Test a trained model's noise dependence.

    Args:
        model: Trained LFCNN model instance.
    """
    # Set default psnr values
    psnr_values = psnr_values or [None, 60, 55, 50, 45, 40, 35, 30, 25, 20, 15, 10, 5]
    
    # Init metrics
    if network["model_type"].lower() == "sparse_coding":
        metrics = dict(light_field=get_lf_metrics())

    elif network["model_type"].lower() == "disparity":
        metrics = dict(disparity=get_disparity_metrics())

    elif network["model_type"].lower() == "center_and_disparity":
        # Update metrics to evaluate full sized MS-SSIM if large enough data
        if settings['test_kwargs']["augmented_shape"][2] >= 128 and \
                settings['test_kwargs']["augmented_shape"][3] >= 128:
            metrics = dict(disparity=get_disparity_metrics(),
                           central_view=get_central_metrics_fullsize())
        else:
            metrics = dict(disparity=get_disparity_metrics(),
                           central_view=get_central_metrics_small())
    else:
        raise ValueError(f"No metrics specified for model type {network['model_type']}.")

    model.set_metrics(metrics)

    # Test
    gen_kwargs = settings['test_kwargs'].copy()

    for i, psnr in enumerate(psnr_values):
        print(f"Testing PSNR {psnr}...")
        gen_kwargs['psnr'] = psnr
        test_res = model.test(**gen_kwargs)
        print("... done.")

        # Log test results to sacred
        print("Logging test results...")
        _run.log_scalar("noise_psnr", psnr, i)
        for key in test_res:
            _run.log_scalar("noise_test_" + key, test_res[key], i)
        print("... done.")

    return


@network.capture
def evaluate(model: BaseModel,
             which: str,
             settings, network, _run, _log):
    """Evaluate a trained model.

    Args:
        model: Trained LFCNN model instance.

        which: Either "challenges" or "wall". Evaluates LFCNN dataset challenges.
    """
    # Init metrics
    if network["model_type"].lower() == "sparse_coding":
        metrics = dict(light_field=get_lf_metrics())

    elif network["model_type"].lower() == "disparity":
        metrics = dict(disparity=get_disparity_metrics())

    elif network["model_type"].lower() == "center_and_disparity":
        # Update metrics to evaluate full sized MS-SSIM if large enough data
        if settings['eval_kwargs']["augmented_shape"][2] >= 128 and \
                settings['eval_kwargs']["augmented_shape"][3] >= 128:
            metrics_central_view = get_central_metrics_fullsize()
            metrics_disp = get_disparity_metrics()
        else:
            metrics_central_view = get_central_metrics_small()
            metrics_disp = get_disparity_metrics()

        if network['model'].lower() == "conv3ddecode2dstcentral":
            metrics = dict(central_view=metrics_central_view)
        elif network['model'].lower() == "conv3ddecode2dstdisp":
            metrics = dict(disparity=metrics_disp)
        else:
            metrics = dict(disparity=metrics_disp,
                           central_view=metrics_central_view)

    else:
        raise ValueError(f"No metrics specified for model type {network['model_type']}.")

    model.set_metrics(metrics)

    which = which.lower()

    # Evaluate challenges, add artifacts
    if which == "challenges":
        kwargs = settings['eval_kwargs']
    elif which == "wall":
        kwargs = settings['eval_wall_kwargs']
    elif which == "real":
        kwargs = settings['predict_kwargs']
    else:
        raise ValueError("Unavailable evaluation option.")

    print("Evaluating challenges...")
    eval_res = model.evaluate_challenges(**kwargs)
    print("... done")

    # Log eval results to sacred
    print("Logging test results...")

    # Iterate over all scenes
    for i, metrics in enumerate(eval_res['metrics']):
        # Iterate over metrics
        for key in metrics:
            _run.log_scalar(f"eval_{which}_{key}", metrics[key], i)
        print("... done.")

    print("Saving results...")
    tempdir_path = Path(os.environ['TMP'])
    predict_path = tempdir_path / f'{which}_predict.npz'
    np.savez(predict_path, **eval_res)
    _run.add_artifact(predict_path)

    # Add PNGs as artifacts for challenges
    if not which == "wall":
        if 'central_view' in eval_res.keys():
            for i in range(len(eval_res['central_view'])):
                # Convert central view reconstruction to RGB and save as PNG
                central_path = tempdir_path/f"{which}_{i + 1}_central_rgb.png"
                SpectralImage(eval_res['central_view'][i]).save_rgb(central_path)
                # Add artifacts
                _run.add_artifact(central_path)

        if 'disparity' in eval_res.keys():
            for i in range(len(eval_res['disparity'])):
                # Create PNG for disparity reconstruction
                disp_path = tempdir_path/f"{which}_{i + 1}_disparity_rgb.png"

                fig = plt.figure(frameon=True)
                fig.set_size_inches(1, 1)
                ax = plt.Axes(fig, [0.08, 0.16, 0.82, 0.82])
                ax.set_axis_off()
                fig.add_axes(ax)
                im = ax.imshow(np.squeeze(eval_res['disparity'][i]), interpolation='none', aspect='auto')
                cb = fig.colorbar(im, orientation='horizontal', cax=fig.add_axes([0.3, 0.1, 0.4, 0.03]))
                cb.ax.tick_params(labelsize=2, size=1, width=0.1, pad=0.2)
                cb.outline.set_linewidth(0.01)
                fig.savefig(disp_path, dpi=512)

                # Add artifacts
                _run.add_artifact(disp_path)

    print("... done.")

    return


@network.capture
def predict(model: BaseModel,
            which: str,
            settings, network, _run, _log):
    """Predict dataset from a trained model.

    Args:
        model: Trained LFCNN model instance.
        which: Which dataset to evaluate. Either "own" or "xiong"
    """
    # Evaluate challenges, add artifacts
    if which == "own":
        kwargs = settings['predict_own_kwargs']
    elif which == "xiong":
        kwargs = settings['predict_xiong_kwargs']
    else:
        raise ValueError("Unavailable evaluation option.")

    # Init metrics
    if network["model_type"].lower() == "sparse_coding":
        metrics = dict(light_field=get_lf_metrics())

    elif network["model_type"].lower() == "disparity":
        metrics = dict()

    elif network["model_type"].lower() == "center_and_disparity":
        from lfcnn.generators import CentralGenerator
        model._generator = CentralGenerator

        # Update metrics to evaluate full sized MS-SSIM if large enough data
        if settings['eval_kwargs']["augmented_shape"][2] >= 128 and \
                settings['eval_kwargs']["augmented_shape"][3] >= 128:
            metrics_central_view = get_central_metrics_fullsize()
            metrics_disp = get_disparity_metrics()
        else:
            metrics_central_view = get_central_metrics_small()
            metrics_disp = get_disparity_metrics()

        if network['model'].lower() == "conv3ddecode2dstcentral":
            metrics = dict(central_view=metrics_central_view)
        elif network['model'].lower() == "conv3ddecode2dstdisp":
            # No labels for real-world data, hence no metrics can be calculated
            metrics = dict(disparity=[])
        else:
            metrics = dict(disparity=metrics_disp,
                           central_view=metrics_central_view)
    else:
        raise ValueError(f"No metrics specified for model type {network['model_type']}.")

    model.set_metrics(metrics)

    # print("Predicting dataset...")
    # pred_res = model.predict(**kwargs)
    # print("... done")

    print("Predicting dataset...")
    pred_res = model.evaluate_challenges(**kwargs)
    print("... done")

    # Calculate metrics for central view (disparity has no ground truth)

    print("Saving results...")

    tempdir_path = Path(os.environ['TMP'])
    predict_path = tempdir_path / f'real_predict_{which}.npz'
    np.savez(predict_path, **pred_res)
    _run.add_artifact(predict_path)

    # Add PNGs as artifacts for data
    if 'central_view' in pred_res.keys():
        for i in range(len(pred_res['central_view'])):
            # Convert central view reconstruction to RGB and save as PNG
            central_path = tempdir_path/f"{which}_{i + 1}_central_rgb.png"
            SpectralImage(pred_res['central_view'][i]).save_rgb(central_path)
            # Add artifacts
            _run.add_artifact(central_path)

    if 'disparity' in pred_res.keys():
        for i in range(len(pred_res['disparity'])):
            # Create PNG for disparity reconstruction
            disp_path = tempdir_path/f"real_{which}_{i + 1}_disparity_rgb.png"

            fig = plt.figure(frameon=True)
            fig.set_size_inches(1, 1)
            ax = plt.Axes(fig, [0.08, 0.16, 0.82, 0.82])
            ax.set_axis_off()
            fig.add_axes(ax)
            im = ax.imshow(np.squeeze(pred_res['disparity'][i]), interpolation='none', aspect='auto')
            cb = fig.colorbar(im, orientation='horizontal', cax=fig.add_axes([0.3, 0.1, 0.4, 0.03]))
            cb.ax.tick_params(labelsize=2, size=1, width=0.1, pad=0.2)
            cb.outline.set_linewidth(0.01)
            fig.savefig(disp_path, dpi=512)

            # Add artifacts
            _run.add_artifact(disp_path)

    print("... done.")

    return


@network.capture
def lr_finder(settings, network, _run, _log):

    # Set up lr finder callback
    lr_min, lr_max = 1e-7, 1e0
    num_batches = int(settings["epochs"] * settings['num_data'] * settings['data_percentage'] / settings["batch_size"])

    lr_finder = LearningRateFinder(lr_min=lr_min, lr_max=lr_max,
                                   num_batches=num_batches,
                                   sweep="exponential",
                                   verbose=False)
    sacred_time_logger = SacredTimeLogger(_run)
    sacred_epoch_logger = SacredEpochLogger(_run, epochs=settings['epochs'])
    callbacks = [lr_finder, sacred_time_logger, sacred_epoch_logger]

    # Get model class
    model = get_model_type(network["model_type"]).get(network["model"])
    model_cls = get_model_cls(network["model_cls"])
    model_cls_kwargs = network["model_cls_kwargs"]

    # Load and init losses
    loss, loss_weights = get_loss_and_weights(network)

    # Load and init optimizer
    optimizer = get_optimizer(network)

    # Init metrics
    metrics = dict(disparity=get_disparity_metrics(),
                   central_view=get_central_metrics_small())

    # Create lfcnn model
    model = model(optimizer=optimizer, loss=loss, metrics=metrics,
                  callbacks=callbacks, loss_weights=loss_weights,
                  model_cls=model_cls, model_cls_kwargs=model_cls_kwargs)

    # Train
    print("Training...")
    hist = model.train(**settings['train_kwargs'])
    print("... done.")

    # Log lr finder results to sacred
    print("Logging results...")
    for i, lr in enumerate(lr_finder.lrs):
        _run.log_scalar("lr", lr, i)
    for i, loss in enumerate(lr_finder.losses):
        _run.log_scalar("loss", loss, i)
    for i, avg in enumerate(lr_finder.avg_losses):
        _run.log_scalar("avg_loss", avg, i)
    print("... done.")


def get_loss_and_weights(network) -> tuple:
    """Get loss and loss_weights from network configuration.

    Args:
        network: Network ingredient.

    Returns:
        Tuple loss, loss_weights
    """

    # For sparse coding networks
    if network['loss_central'] is None and network['loss_disp'] is None and network['loss_lf'] is None:
        loss = None

    elif network['loss_central'] is None and network['loss_disp'] is None:
        loss_lf = losses.get(network['loss_lf'])
        loss_lf = loss_lf(**network['loss_lf_kwargs'])
        loss = dict(light_field=loss_lf)

    # For disparity only networks
    elif network['loss_central'] is None:
        loss_disp = losses.get(network['loss_disp'])
        loss_disp = loss_disp(**network['loss_disp_kwargs'])
        loss = dict(disparity=loss_disp)

    # For central view only networks
    elif network['loss_disp'] is None:
        loss_central = losses.get(network['loss_central'])
        loss_central = loss_central(**network['loss_central_kwargs'])
        loss = dict(central_view=loss_central)

    # For multi-task networks
    else:
        loss_disp = losses.get(network['loss_disp'])
        loss_central = losses.get(network['loss_central'])
        loss_disp = loss_disp(**network['loss_disp_kwargs'])
        loss_central = loss_central(**network['loss_central_kwargs'])
        loss = dict(disparity=loss_disp, central_view=loss_central)

    loss_weights = network['loss_weights']

    return loss, loss_weights


def get_optimizer(network) -> optimizers.Optimizer:
    """Get optimizer instance from network ingredient.

    Args:
        network: Network ingredient.

    Returns:
        Keras optimizer instance.
    """

    optimizer_name = network['optimizer']
    optimizer_kwargs = network['optimizer_kwargs']

    if optimizer_name.lower() == "yogi":
        from tensorflow_addons.optimizers import Yogi
        optimizer = Yogi(optimizer_kwargs['learning_rate'])
    else:
        optimizer_dict = dict(class_name=optimizer_name,
                              config=optimizer_kwargs)
        optimizer = optimizers.get(optimizer_dict)

    return optimizer


def get_sacred_callbacks(_run, epochs, offset=0):
    # Set up callbacks
    sacred_metric_logger = SacredMetricLogger(_run, offset=offset)
    sacred_time_logger = SacredTimeLogger(_run, offset=offset)
    sacred_epoch_logger = SacredEpochLogger(_run, epochs=epochs, offset=offset)
    sacred_lr_logger = SacredLearningRateLogger(_run, offset=offset)
    sacred_loss_weight_logger = SacredLossWeightLogger(_run, offset=offset)

    return [sacred_metric_logger, sacred_time_logger, sacred_epoch_logger, sacred_lr_logger, sacred_loss_weight_logger]


def get_model_weights_path(trained_model, mongo_conf: str = MONGO_CONF) -> List[Path]:
    """Get weights artifact from previously run Sacred experiment.
    Downloaded weights are cached.

    Args:
        trained_model: trained_model Ingredient.

        mongo_conf: Name of the MongoDB config file used by mdbh.

    Returns:
        Path to the model weights.
    """

    db = mdbh.get_mongodb(mongo_conf, trained_model['db_name'])

    ids = trained_model['id']
    weights_names = trained_model['weights_name']

    # Wrap in list
    ids = list(ids) if isinstance(ids, (tuple, list, set)) else [ids]
    weights_names = list(weights_names) if isinstance(weights_names, (tuple, list, set)) else [weights_names]

    return [mdbh.get_artifact(db, id=id, name=name) for id, name in zip(ids, weights_names)]


def get_trained_model(trained_model, network, mongo_conf: str = MONGO_CONF, predict="own"):

    try:
        # Load and init losses
        loss, loss_weights = get_loss_and_weights(network)
        # Load and init optimizer
        optimizer = get_optimizer(network)

        model_kwargs = dict(optimizer=optimizer, loss=loss, loss_weights=loss_weights, metrics=None, callbacks=None)

    except:
        model_kwargs = trained_model['model_kwargs']

    if network['model_kwargs'] is not None:
        model_kwargs = dict(**model_kwargs, **network['model_kwargs'])

    if network['model_cls'] is not None:
        model_kwargs = dict(**model_kwargs, model_cls=get_model_cls(network["model_cls"]), model_cls_kwargs=network['model_cls_kwargs'])

    try:
        if trained_model['model_cls_kwargs_append'] is not None:
            model_kwargs['model_cls_kwargs'] = dict(**model_kwargs['model_cls_kwargs'], **trained_model['model_cls_kwargs_append'])
    except KeyError:
        pass

    model = get_model_type(trained_model['model_type']).get(trained_model['model'])
    model = model(**model_kwargs)
    weights_path = get_model_weights_path(trained_model, mongo_conf)

    if predict == "own":
        gen_shape = network['generated_shape_predict_own']
        aug_shape = network['augmented_shape_predict_own']

    elif predict == "xiong":
        gen_shape = network['generated_shape_predict_xiong']
        aug_shape = network['augmented_shape_predict_xiong']

    else:
        gen_shape = network['generated_shape']
        aug_shape = network['augmented_shape']

    # Load weights, might be multiple
    by_name = trained_model['load_weights_by_name']
    for path in weights_path:
        model.load_weights(str(path),
                           generated_shape=gen_shape,
                           augmented_shape=aug_shape,
                           by_name=by_name)

    return model


def model_save_mask(filename, model, _run, _log):
    # Save the model's mask

    print("Saving model coding mask.")
    mask = model.get_mask()
    tmp_dir = Path(os.environ['TMP'])
    filename = Path(tmp_dir / filename)
    mask_path = str(filename.with_suffix(".npy"))
    np.save(mask_path, mask)
    _run.add_artifact(mask_path)

    # Convert to image
    mask_path_img = str(filename.with_suffix(".png"))
    fig = plt.figure(frameon=True)
    fig.set_size_inches(1, 1)
    ax = plt.Axes(fig, [0.08, 0.16, 0.82, 0.82])
    ax.set_axis_off()
    fig.add_axes(ax)
    im = ax.imshow(np.squeeze(np.argmax(mask, axis=-1)), interpolation='none', aspect='auto')
    fig.savefig(mask_path_img, dpi=512)
    _run.add_artifact(mask_path_img)
