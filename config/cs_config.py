"""Configuration of 5D DCT experiment."""
from lfcnn.utils.tf_utils import use_cpu
use_cpu()

import multiprocessing
import os
from pathlib import Path
import platform
import socket
import subprocess

from mdbh import get_uri

# Optional: Set multithreading
import tensorflow as tf
tf.config.threading.set_inter_op_parallelism_threads(4)
tf.config.threading.set_intra_op_parallelism_threads(4)

import numpy as np
from plenpy.spectral import SpectralImage

from lfcnn.generators.reshapes import lf_identity
from lfcnn.metrics import get_lf_metrics
from lfcnn.metrics import get_central_metrics_fullsize
from lfcnn.metrics import get_central_metrics_small

from sacred import Experiment
from sacred import SETTINGS
from sacred.observers import MongoObserver

from .dataset import dataset
from .settings import settings
from .support.lf_mask_disp_generator import LfMaskGenerator
from .support.reconstruct import CsReconstructor


# Ugly, but globals needed for multiprocessing and pickling...
generator = None
penalty_coeff = None
metrics_fn = None
n_total = None


###################################################
# Define Reconstruction
###################################################

@settings.capture
def reconstruct(settings, dataset, _run, _log, which="challenges"):
    """Reconstruction full size challenges.

    """
    global metrics_fn
    if which == "challenges":
        workers = 2
        augmented_shape = settings["augmented_shape_full"]
        generated_shape = settings["generated_shape_full"]
        data_path = dataset["challenge_path"]
        data_percentage = 1.0
        map_fn = reconstruct_single
        metrics_fn = dict(light_field=get_lf_metrics(), central_view=get_central_metrics_fullsize())

    elif which == "wall":
        workers = 2
        augmented_shape = settings["augmented_shape_full"]
        generated_shape = settings["generated_shape_full"]
        data_path = dataset["challenge_wall_path"]
        data_percentage = 1.0
        map_fn = reconstruct_single
        metrics_fn = dict(light_field=get_lf_metrics(), central_view=get_central_metrics_fullsize())

    elif which == "real_own":
        workers = 2
        augmented_shape = settings["augmented_shape_real_own_full"]
        generated_shape = settings["generated_shape_real_own_full"]
        data_path = dataset["real_own_path"]
        data_percentage = 1.0
        map_fn = reconstruct_single
        metrics_fn = dict(light_field=get_lf_metrics(), central_view=get_central_metrics_fullsize())

    elif which == "test_dataset":
        workers = settings["workers"]
        augmented_shape = settings["augmented_shape"]
        generated_shape = settings["generated_shape"]
        data_path = dataset["test_path"]
        data_percentage = settings["data_percentage"]
        map_fn = reconstruct_single_metric_only
        metrics_fn = dict(light_field=get_lf_metrics(), central_view=get_central_metrics_small())

    else:
        raise ValueError("Unknown option {which}.")

    if which == "test_dataset":
        map_fn = reconstruct_single_metric_only
    else:
        map_fn = reconstruct_single


    gen_kwargs = dict(data=data_path,
                      data_key="lf",
                      label_keys=None,
                      augmented_shape=augmented_shape,  # Angular and spatial crop
                      generated_shape=generated_shape,  # Identity, sanity check
                      reshape_func=lf_identity,
                      batch_size=1,
                      range_data=dataset["range_data"],
                      data_percentage=data_percentage,
                      augment=False,
                      shuffle=False,
                      use_mask=True,
                      fix_seed=True)

    global generator
    generator = LfMaskGenerator(**gen_kwargs)

    global penalty_coeff
    penalty_coeff = settings["penalty_coeff"]

    global n_total
    n_total = len(generator)
    print(f"Reconstructing {n_total} light fields of size {generated_shape}.")

    # Create multiprocessing pool for parallel reconstruction
    p = multiprocessing.Pool(processes=workers)
    res_all = p.map(map_fn, range(len(generator)))
    p.close()

    print(f"Reconstruction successful.")

    if which != "test_dataset":
        reconstruction = [i[0] for i in res_all]
        metrics = [i[1] for i in res_all]
        u, v, s, t, ch = reconstruction[0].shape
        u_c, v_c = u//2, v//2
        central_view = [lf[u_c, v_c] for lf in reconstruction]
        tempdir_path = Path(os.environ['TMP'])
        predict_path = tempdir_path/f'{which}_predict.npz'
        eval_res = dict(light_field=reconstruction,
                        central_view=central_view,
                        metrics=metrics)
        np.savez(predict_path, **eval_res)
        _run.add_artifact(predict_path)

        # Artifact too large for MongoDB
        # Copy to homefolder
        if settings['slurm_job_id'] is not None:
            id = settings['slurm_job_id']
        else:
            import uuid
            id = str(uuid.uuid4())
            id_path = tempdir_path/f'{which}_predict_id_{id}.txt'
            args = ["touch", str(id_path)]
            subprocess.run(args)
            _run.add_artifact(id_path)
                trgt_path = Path(os.environ['HOME']) / Path(f"artifact_cs_reconstruct_{which}_id_{id}.npz")
        args = ["cp", str(predict_path), str(trgt_path)]
        subprocess.run(args)

        if not which == "wall":
            for i in range(len(eval_res['light_field'])):
                central_path = tempdir_path/f"{which}_{i + 1}_central_rgb.png"

                # Convert central view reconstruction to RGB and save as PNG
                SpectralImage(eval_res['central_view'][i]).save_rgb(central_path)

                # Add artifacts
                _run.add_artifact(central_path)

    else:
        # which == "test_dataset":
        # Calculate mean metric values
        test_res = {name: np.mean([d[name] for d in res_all])
                    for name in res_all[0]}

        print("Logging test results...")
        for key in test_res:
            _run.log_scalar("test_" + key, test_res[key], 0)
        print("... done.")

    return


def reconstruct_single(i):
    global generator
    global penalty_coeff
    global metrics_fn
    global n_total

    lf_code, labels = generator.__getitem__(i)
    lf_org = labels["light_field"]
    mask = labels["mask"]

    print(f"Starting reconstruction {i+1} of {n_total}...")
    reconstructor = CsReconstructor(np.squeeze(lf_org),
                                    np.squeeze(lf_code),
                                    np.squeeze(mask),
                                    penalty_coeff,
                                    metrics_fn)
    lf_recon, metrics = reconstructor.run()
    print(f"...reconstruction {i+1} of {n_total} complete")
    return lf_recon, metrics


def reconstruct_single_metric_only(i):
    global generator
    global penalty_coeff
    global metrics_fn
    global n_total

    print(f"Starting reconstruction {i+1} of {n_total}...")
    lf_code, labels = generator.__getitem__(i)
    lf_org = labels["light_field"]
    mask = labels["mask"]
    reconstructor = CsReconstructor(np.squeeze(lf_org),
                                    np.squeeze(lf_code),
                                    np.squeeze(mask),
                                    penalty_coeff,
                                    metrics_fn)
    lf_recon, metrics = reconstructor.run()
    print(f"...reconstruction {i+1} of {n_total} complete")
    return metrics
