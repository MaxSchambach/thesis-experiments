"""
Definitions of multi-dimensional DCT transforms.
While scipy also provides a dctn implmentation, this was not compatible
with liblbfgs/pylbfgs.
"""

import scipy.fftpack as spfft


def dctn(x):
    n = x.ndim
    if n == 2:
        return dct2(x)
    elif n == 3:
        return dct3(x)
    elif n == 5:
        return dct5(x)
    else:
        raise ValueError(f"Incompatible dimension {n}.")


def idctn(x):
    n = x.ndim
    if n == 2:
        return idct2(x)
    elif n == 3:
        return idct3(x)
    elif n == 5:
        return idct5(x)
    else:
        raise ValueError(f"Incompatible dimension {n}.")


def dct2(x):
    return spfft.dct(spfft.dct(x, norm='ortho', axis=0),
                     norm='ortho', axis=1)


def idct2(x):
    return spfft.idct(spfft.idct(x, norm='ortho', axis=1),
                      norm='ortho', axis=0)


def dct3(x):
    return spfft.dct(spfft.dct(spfft.dct(x, norm='ortho', axis=0),
                               norm='ortho', axis=1),
                     norm='ortho', axis=2)


def idct3(x):
    return spfft.idct(spfft.idct(spfft.idct(x, norm='ortho', axis=2),
                                 norm='ortho', axis=1),
                      norm='ortho', axis=0)


def dct4(x):
    return spfft.dct(spfft.dct(spfft.dct(spfft.dct(x, norm='ortho', axis=0),
                                         norm='ortho', axis=1),
                               norm='ortho', axis=2),
                     norm='ortho', axis=3)


def idct4(x):
    return spfft.idct(spfft.idct(spfft.idct(spfft.idct(x, norm='ortho', axis=3),
                                            norm='ortho', axis=2),
                                 norm='ortho', axis=1),
                      norm='ortho', axis=0)


def dct5(x):
    return spfft.dct(spfft.dct(spfft.dct(spfft.dct(spfft.dct(x, norm='ortho', axis=0),
                                                   norm='ortho', axis=1),
                                         norm='ortho', axis=2),
                               norm='ortho', axis=3),
                     norm='ortho', axis=4)


def idct5(x):
    return spfft.idct(spfft.idct(spfft.idct(spfft.idct(spfft.idct(x, norm='ortho', axis=4),
                                                       norm='ortho', axis=3),
                                            norm='ortho', axis=2),
                                 norm='ortho', axis=1),
                      norm='ortho', axis=0)
