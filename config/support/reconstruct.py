"""

"""
from tensorflow.python.framework import ops as tf_ops
import tensorflow.keras.backend as K

import numpy as np

from pylbfgs import owlqn
from tensorflow.keras.losses import Loss

from .dctn import dct2
from .dctn import idct2
from .dctn import dct3
from .dctn import idct3
from .dctn import dct4
from .dctn import idct4
from .dctn import dct5
from .dctn import idct5


class CsReconstructor:
    def __init__(self, original, measurement, mask, penalty_coff, metrics):
        self.original = original
        self.measurement = measurement.astype(np.float32)
        self.shape = measurement.shape
        self.ndim = measurement.ndim
        self.recon_img = None
        self.penalty_coff = penalty_coff
        self.mask = mask
        self.mask_ch = np.zeros((measurement.shape[0], measurement.shape[1]))
        # Create new metric instances
        self.metrics = {i: [j.__class__.from_config(j.get_config()) for j in metrics[i]] for i in metrics}

    def evaluate(self, x, g, step):

        # expand x columns-first
        x_reshaped = x.reshape(self.shape)

        # Ax is just the inverse 2D dct of x2
        Ax = self.decode(x_reshaped)

        # stack columns and extract samples, Ax in (u,v)
        Ax = np.multiply(Ax, self.mask)

        # calculate the residual Ax-b and its 2-norm squared
        Ax_b = np.subtract(Ax, self.measurement)
        fx = np.sum(np.power(Ax_b, 2))

        # A'(Ax-b) is just the 2D dct of Axb2
        AtAx_b = np.multiply(2, self.encode(Ax_b))
        AtAx_b = AtAx_b.reshape(x.shape)

        # copy over the gradient vector
        np.copyto(g, AtAx_b)

        return fx

    def reconstruct(self):
        if self.ndim == 2:
            self.encode = dct2
            self.decode = idct2

        elif self.ndim == 3:
            self.encode = dct3
            self.decode = idct3

        elif self.ndim == 4:
            self.encode = dct4
            self.decode = idct4

        elif self.ndim == 5:
            self.encode = dct5
            self.decode = idct5

        else:
            assert "Incompatible number of dimensions."

        # perform the L1 minimization in memory
        x_opt = owlqn(np.prod(self.shape), self.evaluate, progress, self.penalty_coff)

        # transform the output back into the spatial domain
        x_opt = x_opt.reshape(self.shape)  # stack columns
        x_recon = self.decode(x_opt)
        img_recon = x_recon
        return img_recon

    def evaluate_metrics(self, reconstruction):

        # Calculate metric scores for every challenge separately
        eval_res = dict()

        # Calculate light field metrics
        y_pred = tf_ops.convert_to_tensor_v2(reconstruction, dtype=K.floatx())
        y_true = tf_ops.convert_to_tensor_v2(self.original, dtype=K.floatx())
        tf_ops.convert_to_tensor_v2(reconstruction, dtype=K.floatx())
        for metric in self.metrics["light_field"]:
            res = metric(y_true, y_pred).numpy()
            metric_name = metric.name
            metric_name = "light_field_" + metric_name.replace("light_field", "")
            eval_res[metric_name] = res

        # Calculate central_view metrics
        u, v, s, t, ch = self.original.shape
        u_c, v_c = u//2, v//2
        y_pred = tf_ops.convert_to_tensor_v2(reconstruction[u_c, v_c], dtype=K.floatx())
        y_true = tf_ops.convert_to_tensor_v2(self.original[u_c, v_c], dtype=K.floatx())
        for metric in self.metrics["central_view"]:
            res = metric(y_true, y_pred).numpy()
            metric_name = metric.name
            metric_name = "central_view_" + metric_name.replace("central_view", "")
            eval_res[metric_name] = res

        return eval_res

    def run(self):
        """Always returns reconstructed image or light field, and metrics of image or the central view
        of light field."""

        reconstruction = self.reconstruct()
        metrics_eval = self.evaluate_metrics(reconstruction)
        return reconstruction, metrics_eval


def progress(x, g, fx, xnorm, gnorm, step, k, ls):
    # Print variables to screen or file or whatever. Return zero to
    # continue algorithm; non-zero will halt execution.
    return 0
