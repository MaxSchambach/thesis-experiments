from typing import List, Tuple

import numpy as np

from lfcnn.generators.abstracts import BaseGenerator
from lfcnn.generators.utils import AugmentResult
from lfcnn.generators.utils import disp_batch_augment
from lfcnn.generators.utils import get_batch_seeds
from lfcnn.generators.utils import lf_batch_augment
from lfcnn.generators.utils import _create_lf_mask


class LfMaskGenerator(BaseGenerator):
    """Generates light field batches and light field, mask labels."""
    def __init__(self, *args, **kwargs):
        super(LfMaskGenerator, self).__init__(*args, **kwargs)
        self._label_names = ["light_field", "mask"]

    def process_data(self,
                     lf_batch: any,
                     labels: List[any],
                     curr_indices: List[int]) -> Tuple[any, List[any]]:
        # Create empty augmentation result instance
        aug_res = AugmentResult()

        lf_batch = lf_batch_augment(lf_batch=lf_batch,
                                    augmented_shape=self.augmented_shape,
                                    curr_indices=curr_indices,
                                    fix_seed=self.fix_seed,
                                    augment=self.augment,
                                    aug_res=aug_res)

        mask = np.zeros(lf_batch.shape)
        _, _, _, s, t, num_ch = lf_batch.shape

        batch_seeds = get_batch_seeds(curr_indices=curr_indices, fix_seed=self.fix_seed, offset=19)

        for i, seed in enumerate(batch_seeds):
            mask[i] = _create_lf_mask(s, t, num_ch, seed=seed)

        return lf_batch, [lf_batch.copy(), mask]


class LfMaskDispGenerator(BaseGenerator):
    """Generates light field batches and light field, mask labels."""
    def __init__(self, *args, **kwargs):
        super(LfMaskDispGenerator, self).__init__(*args, **kwargs)
        self._label_names = ["light_field", "mask", "disparity"]

    def process_data(self,
                     lf_batch: any,
                     labels: List[any],
                     curr_indices: List[int]) -> Tuple[any, List[any]]:
        # Create empty augmentation result instance
        aug_res = AugmentResult()

        lf_batch = lf_batch_augment(lf_batch=lf_batch,
                                    augmented_shape=self.augmented_shape,
                                    curr_indices=curr_indices,
                                    fix_seed=self.fix_seed,
                                    augment=self.augment,
                                    aug_res=aug_res)

        # Perform corresponding disparity batch augmentation
        disp_batch = disp_batch_augment(disp_batch=labels[0],
                                        augment=self.augment,
                                        aug_res=aug_res)

        mask = np.zeros(lf_batch.shape)
        _, _, _, s, t, num_ch = lf_batch.shape

        batch_seeds = get_batch_seeds(curr_indices=curr_indices, fix_seed=self.fix_seed, offset=19)

        for i, seed in enumerate(batch_seeds):
            mask[i] = _create_lf_mask(s, t, num_ch, seed=seed)

        return lf_batch, [lf_batch.copy(), mask, disp_batch]
