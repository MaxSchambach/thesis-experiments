"""Base settings."""
import os
import socket

from sacred import Ingredient

from .dataset import dataset
from .network import network

# Get current host
HOSTNAME = socket.gethostname()

# Define Ingredients and configurations
settings = Ingredient("settings", ingredients=[dataset, network])


@settings.config
def default(dataset, network):

    data_key = "lf"
    label_key = "disp"

    gpus = 1
    cpu_merge = False

    batch_size = 128
    epochs = 170
    save_weights = True

    augment = False
    use_mask = True
    mask_type = "random"
    psnr = None

    num_data = dataset['num_data']
    valid_batch_size = batch_size
    test_batch_size = batch_size
    data_percentage = 1
    valid_percentage = 1
    verbose = 0
    fix_seed_test = True
    fix_seed_eval = True
    shuffle = True
    mixed_precision = False

    validation_freq = 1
    use_multiprocessing = True

    workers = 4
    max_queue_size = 4

    # Try to set SLURM variables from environment
    try:
        slurm_cpus_per_task = os.environ['SLURM_JOB_CPUS_PER_NODE']
    except KeyError:
        slurm_cpus_per_task = None

    try:
        slurm_mkl_threads = os.environ['MKL_NUM_THREADS']
    except KeyError:
        slurm_mkl_threads = None

    try:
        slurm_job_id = os.environ['SLURM_JOB_ID']
    except KeyError:
        slurm_job_id = None

    # Setup train kwargs to pass to model.train()
    train_kwargs = dict(data=dataset["train_path"],
                        valid_data=dataset['valid_path'],
                        data_key=data_key,
                        label_keys=label_key,
                        augmented_shape=network["augmented_shape"],
                        generated_shape=network["generated_shape"],
                        range_data=dataset["range_data"],
                        range_valid_data=dataset["range_data"],
                        batch_size=batch_size,
                        valid_batch_size=valid_batch_size,
                        epochs=epochs,
                        data_percentage=data_percentage,
                        valid_percentage=valid_percentage,
                        validation_freq=validation_freq,
                        shuffle=shuffle,
                        augment=augment,
                        gpus=gpus,
                        cpu_merge=cpu_merge,
                        gen_kwargs=network["gen_kwargs"],
                        use_mask=use_mask,
                        mask_type=mask_type,
                        psnr=psnr,
                        workers=workers,
                        max_queue_size=max_queue_size,
                        use_multiprocessing=use_multiprocessing,
                        verbose=verbose
                        )

    # Setup test kwargs to pass to model.test()
    test_kwargs = dict(data=dataset["test_path"],
                       data_key=data_key,
                       label_keys=label_key,
                       augmented_shape=network["augmented_shape"],
                       generated_shape=network["generated_shape"],
                       range_data=dataset["range_data"],
                       batch_size=test_batch_size,
                       data_percentage=data_percentage,
                       gpus=gpus,
                       cpu_merge=cpu_merge,
                       gen_kwargs=network["gen_kwargs"],
                       use_mask=use_mask,
                       mask_type=mask_type,
                       psnr=psnr,
                       fix_seed=fix_seed_test,
                       workers=workers,
                       max_queue_size=max_queue_size,
                       use_multiprocessing=use_multiprocessing,
                       verbose=verbose
                       )

    # Setup test kwargs to pass to model.evaluate_challenges()
    eval_kwargs = dict(data=dataset["challenge_path"],
                       data_key=data_key,
                       label_keys=label_key,
                       augmented_shape=network["augmented_shape_full"],
                       generated_shape=network["generated_shape_full"],
                       range_data=dataset["range_data"],
                       gen_kwargs=network["gen_kwargs"],
                       use_mask=use_mask,
                       mask_type=mask_type,
                       psnr=psnr,
                       fix_seed=fix_seed_eval,
                       workers=workers,
                       max_queue_size=max_queue_size,
                       use_multiprocessing=use_multiprocessing,
                       verbose=verbose
                       )

    # Setup test kwargs to pass to model.evaluate_challenges()
    eval_wall_kwargs = eval_kwargs.copy()
    eval_wall_kwargs["data"] = dataset["challenge_wall_path"]

    # Setup predict kwargs to pass to model.predict
    predict_own_kwargs = dict(data=dataset["real_own_path"],
                              data_key=data_key,
                              label_keys=None,
                              augmented_shape=network["augmented_shape_predict_own"],
                              generated_shape=network["generated_shape_predict_own"],
                              range_data=dataset["range_data"],
                              gen_kwargs=network["gen_kwargs"],
                              use_mask=use_mask,
                              mask_type=mask_type,
                              psnr=psnr,
                              fix_seed=fix_seed_eval,
                              workers=workers,
                              max_queue_size=max_queue_size,
                              use_multiprocessing=use_multiprocessing,
                              verbose=verbose
                              )

    predict_xiong_kwargs = dict(data=dataset["real_xiong_path"],
                                data_key=data_key,
                                label_keys=None,
                                augmented_shape=network["augmented_shape_predict_xiong"],
                                generated_shape=network["generated_shape_predict_xiong"],
                                range_data=dataset["range_data"],
                                gen_kwargs=network["gen_kwargs"],
                                use_mask=use_mask,
                                mask_type=mask_type,
                                psnr=psnr,
                                fix_seed=fix_seed_eval,
                                workers=workers,
                                max_queue_size=max_queue_size,
                                use_multiprocessing=use_multiprocessing,
                                verbose=verbose
                                )


@settings.named_config
def no_augment():
    augment = dict(flip=False,
                   rotate=False,
                   weigh_chs=False,
                   gamma=False,
                   permute_chs=False,
                   scale=False)


@settings.named_config
def augment():
    augment = dict(flip=True,
                   rotate=True,
                   weigh_chs=True,
                   gamma=True,
                   permute_chs=True,
                   scale=True)


@settings.named_config
def mixed_precision():
    from lfcnn.utils import tf_utils
    tf_utils.set_mixed_precision_keras(policy='mixed_float16',
                                       loss_scale='dynamic')
    mixed_precision = True


@settings.named_config
def mask_optimization():
    epochs_warmup = 0
    epochs = 35
    epochs_finetune = 15

    # Disable mask in generator since it is applied in the network
    use_mask = False
    mask_type = None
