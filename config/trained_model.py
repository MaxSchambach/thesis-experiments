"""Base model weights definitions."""
from tensorflow.keras.optimizers import SGD
from sacred import Ingredient

# Define trained_model Ingredient and configurations
trained_model = Ingredient("trained_model")


@trained_model.config
def default():
    model = None
    # Use dummy optimizer
    model_kwargs = dict(optimizer=SGD(), loss=None, metrics=None, callbacks=None)
    model_type = None
    db_name = None
    id = None
    load_weights_by_name = False


@trained_model.named_config
def cs_vector_dict_epinet():
    model = "dictionarysparsecodingepinet"
    model_type = "center_and_disparity"

    db_name = "sacred"
    # IDs of the learned dictionary and trained EPINET
    id = [<CsVectorDictTrain-id>, <Epinet-id>]
    weights_name = ['weights.h5', 'weights.h5']
    load_weights_by_name = True


@trained_model.named_config
def cs_tensor_dict_epinet():
    model = "dictionarysparsecodingepinet"
    model_type = "center_and_disparity"

    db_name = "sacred"
    # IDs of the learned dictionary and trained EPINET
    id = [<CsTensorDictTrain-id>, <Epinet-id>]
    weights_name = ['weights.h5', 'weights.h5']
    load_weights_by_name = True



@trained_model.named_config
def epinet():
    model = "epinet"
    model_type = "disparity"

    db_name = "sacred"
    id = <Epinet-id>
    weights_name = 'weights.h5'
    load_weights_by_name = False


@trained_model.named_config
def mt_uncertainty_al_norm_grad_sim():
    model = "conv3ddecode2d"
    model_type = "center_and_disparity"

    db_name = "sacred"
    id = <MtUncertaintyAlNormGradSim-id>
    weights_name = 'weights_renamed.h5'
    load_weights_by_name = True
