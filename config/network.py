"""Base network definitions."""
import inspect

from sacred import Ingredient

from lfcnn.models import get_model_type

# Define NETWORK Ingredient and configurations
network = Ingredient("network")


@network.config
def default():
    model = None
    model_kwargs = dict()
    model_type = None
    model_cls = None
    model_cls_kwargs = None

    # Add source code of the specified model
    if model_type is not None and model is not None:
        model_source = inspect.getfile(get_model_type(model_type).get(model).create_model)
        network.add_source_file(model_source)

    loss_disp = None
    loss_disp_kwargs = None
    loss_central = None
    loss_central_kwargs = None
    loss_weights = None
    multitask_uncertainty = False

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = None
    generated_shape_full = None
    generated_shape_predict = None
    gen_kwargs = None

    optimizer = "Yogi"
    lr = 5e-3
    optimizer_kwargs = dict(learning_rate=lr)
    schedule = "Sigmoid"

    if optimizer.lower() == "sgd" and schedule is not None and schedule.lower() == "1cycle":
        lr_min = lr
        lr_max = 10.0*lr
        lr_final = lr/1000.0
        optimizer_kwargs = dict(learning_rate=lr, momentum=0.95)

    elif optimizer.lower() == "yogi":
        optimizer_kwargs = dict(learning_rate=lr)
        schedule = "Sigmoid"

    elif optimizer.lower() == "rmsprop":
        optimizer_kwargs = dict(learning_rate=lr)
        schedule = None


@network.named_config
def model_conv3d():
    model = "conv3ddecode2d"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    generated_shape = (32, 32, 9*9, 13)
    generated_shape_full = (512, 512, 9*9, 13)
    generated_shape_predict_own = (400, 400, 9*9, 13)
    generated_shape_predict_xiong = (256, 360, 9*9, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_st_disp():
    model = "conv3ddecode2dstdisp"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    generated_shape = (32, 32, 9*9, 13)
    generated_shape_full = (512, 512, 9*9, 13)
    generated_shape_predict_own = (400, 400, 9*9, 13)
    generated_shape_predict_xiong = (256, 360, 9*9, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_st_central():
    model = "conv3ddecode2dstcentral"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    generated_shape = (32, 32, 9*9, 13)
    generated_shape_full = (512, 512, 9*9, 13)
    generated_shape_predict_own = (400, 400, 9*9, 13)
    generated_shape_predict_xiong = (256, 360, 9*9, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_7x7():
    model = "conv3ddecode2d"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    augmented_shape = (7, 7, 32, 32, 13)
    augmented_shape_full = (7, 7, 512, 512, 13)
    augmented_shape_predict_own = (7, 7, 400, 400, 13)
    augmented_shape_predict_xiong = (7, 7, 256, 360, 13)

    generated_shape = (32, 32, 7*7, 13)
    generated_shape_full = (512, 512, 7*7, 13)
    generated_shape_predict_own = (400, 400, 7*7, 13)
    generated_shape_predict_xiong = (256, 360, 7*7, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_5x5():
    model = "conv3ddecode2d"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    augmented_shape = (5, 5, 32, 32, 13)
    augmented_shape_full = (5, 5, 512, 512, 13)
    augmented_shape_predict_own = (5, 5, 400, 400, 13)
    augmented_shape_predict_xiong = (5, 5, 256, 360, 13)

    generated_shape = (32, 32, 5*5, 13)
    generated_shape_full = (512, 512, 5*5, 13)
    generated_shape_predict_own = (400, 400, 5*5, 13)
    generated_shape_predict_xiong = (256, 360, 5*5, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_3x3():
    model = "conv3ddecode2d"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    augmented_shape = (3, 3, 32, 32, 13)
    augmented_shape_full = (3, 3, 512, 512, 13)
    augmented_shape_predict_own = (3, 3, 400, 400, 13)
    augmented_shape_predict_xiong = (3, 3, 256, 360, 13)

    generated_shape = (32, 32, 3*3, 13)
    generated_shape_full = (512, 512, 3*3, 13)
    generated_shape_predict_own = (400, 400, 3*3, 13)
    generated_shape_predict_xiong = (256, 360, 3*3, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_mask_fractal():
    model = "conv3ddecode2dmasked"
    model_type = "center_and_disparity"
    mask_type = "neural_fractal"
    lambda_e_init = 1e-3
    lambda_e_end = 1e3
    inv_temp_init = 1.0
    inv_temp_end = 200.0
    mask_kwargs = dict(pattern_shape=(4, 4),
                       inv_temp=5.0,
                       hard_forward=True,
                       lambda_e=1e-4,
                       lambda_s=None,
                       regular=False,
                       initializer='truncated_normal',
                       compress="full",
                       roll=True)
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5, mask_type=mask_type, mask_kwargs=mask_kwargs)

    generated_shape = (9, 9, 32, 32, 13)
    generated_shape_full = (9, 9, 512, 512, 13)
    generated_shape_predict_own = (9, 9, 400, 400, 13)
    generated_shape_predict_xiong = (9, 9, 256, 360, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_mask_baseline():
    model = "conv3ddecode2dmasked"
    model_type = "center_and_disparity"
    lr = 1e-3
    mask_type = "dummy"
    mask_kwargs = dict()
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5, mask_type=mask_type, mask_kwargs=mask_kwargs)

    generated_shape = (9, 9, 32, 32, 13)
    generated_shape_full = (9, 9, 512, 512, 13)
    generated_shape_predict_own = (9, 9, 400, 400, 13)
    generated_shape_predict_xiong = (9, 9, 256, 360, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv4d():
    model = "conv4ddecode2d"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24, skip=True, kernel_reg=1e-5)

    generated_shape = (9*9, 32, 32, 13)
    generated_shape_full = (9*9, 512, 512, 13)
    generated_shape_predict_own = (9*9, 400, 400, 13)
    generated_shape_predict_xiong = (9*9, 256, 360, 13)
    gen_kwargs = dict()


@network.named_config
def model_conv3d_superresolution():
    model = "conv3ddecode2d"
    model_type = "center_and_disparity"
    model_kwargs = dict(num_filters_base=24,
                        skip=True,
                        superresolution=True,
                        kernel_reg=1e-5)

    generated_shape = (16, 16, 9*9, 13)
    generated_shape_full = (256, 256, 9*9, 13)
    generated_shape_predict_own = (200, 200, 9*9, 13)
    generated_shape_predict_xiong = (128, 180, 9*9, 13)
    gen_kwargs = dict(downsample=True)


@network.named_config
def model_epinet():
    model = "epinet"
    model_type = "disparity"
    model_kwargs = dict()
    # Value in reference implementation differs from that in paper
    lr = 1e-4

    augmented_shape = (9, 9, 32, 32, 3)
    augmented_shape_full = (9, 9, 512, 512, 3)
    augmented_shape_predict_own = (9, 9, 400, 400, 3)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 3)
    generated_shape = [(32, 32, 9, 1) for _ in range(4)]
    generated_shape_full = [(512, 512, 9, 1) for _ in range(4)]
    generated_shape_predict_own = [(400, 400, 9, 1) for _ in range(4)]
    generated_shape_predict_xiong = [(256, 360, 9, 1) for _ in range(4)]
    gen_kwargs = dict(to_mono=True)


@network.named_config
def cs_vector_dict_train():
    model = "dictionarysparsecoding"
    model_type = "sparse_coding"
    # Train with spatially non-overlapping patches
    # Otherwise, depatching allocates too much memory to be trained on GPU
    model_kwargs = dict(overcompleteness=2.0,
                        use_mask=False,
                        patch_size_st=(8, 8),
                        patch_size_uv=(5, 5),
                        patch_step_st=(8, 8),
                        patch_step_uv=(4, 4),
                        couple_strength=1e-1,
                        iterations_fista=75,
                        iterations_eigenval=35,
                        efficient_depatch=False,
                        measure_sparsity=True)
    lr = 10.0
    optimizer = "SGD"
    optimizer_kwargs = dict(learning_rate=lr, momentum=0.95)
    schedule = None

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = augmented_shape
    generated_shape_full = augmented_shape_full
    generated_shape_predict_own = augmented_shape_predict_own
    generated_shape_predict_xiong = augmented_shape_predict_xiong


@network.named_config
def cs_tensor_dict_train():
    model = "dictionarysparsecoding"
    model_type = "sparse_coding"
    # Train with spatially non-overlapping patches
    # Otherwise, depatching allocates too much memory to be trained on GPU
    model_kwargs = dict(overcompleteness=2.0,
                        use_mask=False,
                        patch_size_st=(16, 16),
                        patch_size_uv=(7, 7),
                        patch_step_st=(16, 16),
                        patch_step_uv=(2, 2),
                        couple_strength=1e-2,
                        iterations_fista=100,
                        iterations_eigenval=35,
                        efficient_depatch=False,
                        decomposition='tensor_decomposition',
                        measure_sparsity=True,
                        vectorized=True,
                        parallel_iterations=None)
    lr = 1
    # optimizer = "SGD"
    # optimizer_kwargs = dict(learning_rate=lr, momentum=0.95)
    optimizer = "Yogi"
    optimizer_kwargs = dict(learning_rate=lr)
    schedule = None

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = augmented_shape
    generated_shape_full = augmented_shape_full
    generated_shape_predict_own = augmented_shape_predict_own
    generated_shape_predict_xiong = augmented_shape_predict_xiong



@network.named_config
def cs_tensor_dict_epinet_test():
    model = "dictionarysparsecodingepinet"
    model_type = "center_and_disparity"

    sparse_coding_kwargs = dict(overcompleteness=2.0,
                                use_mask=True,
                                patch_size_st=(16, 16),
                                patch_size_uv=(7, 7),
                                patch_step_st=(8, 8),
                                patch_step_uv=(2, 2),
                                couple_strength=1e-2,
                                iterations_fista=100,
                                iterations_eigenval=50,
                                efficient_depatch=True,
                                decomposition='tensor_decomposition',
                                measure_sparsity=True,
                                vectorized=False,
                                parallel_iterations=10)
    model_kwargs = dict(sparse_coding_kwargs=sparse_coding_kwargs)

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = augmented_shape
    generated_shape_full = augmented_shape_full
    generated_shape_predict_own = augmented_shape_predict_own
    generated_shape_predict_xiong = augmented_shape_predict_xiong


@network.named_config
def cs_vector_dict_epinet_test():
    model = "dictionarysparsecodingepinet"
    model_type = "center_and_disparity"

    sparse_coding_kwargs = dict(overcompleteness=2.0,
                                use_mask=True,
                                patch_size_st=(8, 8),
                                patch_size_uv=(5, 5),
                                patch_step_st=(4, 4),
                                patch_step_uv=(4, 4),
                                couple_strength=1e-2,
                                iterations_fista=100,
                                iterations_eigenval=50,
                                efficient_depatch=False,
                                measure_sparsity=True)
    model_kwargs = dict(sparse_coding_kwargs=sparse_coding_kwargs)

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = augmented_shape
    generated_shape_full = augmented_shape_full
    generated_shape_predict_own = augmented_shape_predict_own
    generated_shape_predict_xiong = augmented_shape_predict_xiong


@network.named_config
def cs_vector_dict_epinet_eval():
    model = "dictionarysparsecodingepinet"
    model_type = "center_and_disparity"

    sparse_coding_kwargs = dict(overcompleteness=2.0,
                                use_mask=True,
                                patch_size_st=(8, 8),
                                patch_size_uv=(5, 5),
                                patch_step_st=(4, 4),
                                patch_step_uv=(4, 4),
                                couple_strength=1e-2,
                                iterations_fista=100,
                                iterations_eigenval=50,
                                efficient_depatch=True,
                                measure_sparsity=False)
    model_kwargs = dict(sparse_coding_kwargs=sparse_coding_kwargs)

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = augmented_shape
    generated_shape_full = augmented_shape_full
    generated_shape_predict_own = augmented_shape_predict_own
    generated_shape_predict_xiong = augmented_shape_predict_xiong


@network.named_config
def cs_tensor_dict_epinet_eval():
    model = "dictionarysparsecodingepinet"
    model_type = "center_and_disparity"

    sparse_coding_kwargs = dict(overcompleteness=2.0,
                                use_mask=True,
                                patch_size_st=(16, 16),
                                patch_size_uv=(7, 7),
                                patch_step_st=(8, 8),
                                patch_step_uv=(2, 2),
                                couple_strength=1e-2,
                                iterations_fista=100,
                                iterations_eigenval=50,
                                efficient_depatch=True,
                                decomposition='tensor_decomposition',
                                measure_sparsity=False,
                                vectorized=True,
                                parallel_iterations=None)
    model_kwargs = dict(sparse_coding_kwargs=sparse_coding_kwargs)

    augmented_shape = (9, 9, 32, 32, 13)
    augmented_shape_full = (9, 9, 512, 512, 13)
    augmented_shape_predict_own = (9, 9, 400, 400, 13)
    augmented_shape_predict_xiong = (9, 9, 256, 360, 13)
    generated_shape = augmented_shape
    generated_shape_full = augmented_shape_full
    generated_shape_predict_own = augmented_shape_predict_own
    generated_shape_predict_xiong = augmented_shape_predict_xiong


@network.named_config
def loss_epinet():
    loss_disp = "MAE"
    loss_disp_kwargs = dict()
    loss_central = None
    loss_central_kwargs = None
    loss_weights = None


@network.named_config
def loss_multi_task():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_weights = dict(disparity=0.5, central_view=0.5)


@network.named_config
def loss_disparity_single():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    # Add dummy central_view loss
    loss_central = "MeanSquaredError"
    loss_central_kwargs = dict()
    loss_weights = dict(disparity=1.0, central_view=0.0)


@network.named_config
def loss_central_single():
    # Add dummy disparity loss
    loss_disp = "MeanSquaredError"
    loss_disp_kwargs = dict()
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_weights = dict(disparity=0.0, central_view=1.0)


@network.named_config
def loss_disparity_single_grad_sim():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "GradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"]))


@network.named_config
def loss_central_single_grad_sim():
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "GradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]))


@network.named_config
def loss_disparity_single_norm_grad_sim():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"]),
                            gradient_approximation=None)


@network.named_config
def loss_central_single_norm_grad_sim():
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation=None)


@network.named_config
def loss_grad_norm():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "GradNorm"
    model_cls_kwargs = dict(alpha=0.75, min_val=1e-5)


@network.named_config
def loss_multi_task_uncertainty():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "MultiTaskUncertainty"
    model_cls_kwargs = None


@network.named_config
def loss_gradient_similarity():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "GradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]))


@network.named_config
def loss_normalized_gradient_similarity():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation=None)
@network.named_config
def loss_normalized_gradient_similarity_observe():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation=None,
                            log_dir="/tmp/tensorboard/full_gradients")


@network.named_config
def loss_normalized_gradient_similarity_multi_task_uncertainty():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation", "DisparityNormalSimilarity"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation="None",
                            multi_task_uncertainty=True)


@network.named_config
def loss_approximate_gradient_similarity():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation="every_twentieth")


@network.named_config
def loss_stochastic_gradient_similarity():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation="stochastic",
                            approximation_percentage=0.0258)


@network.named_config
def loss_approximate_gradient_similarity_multi_task_uncertainty():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    model_cls = "NormalizedGradientSimilarity"
    model_cls_kwargs = dict(aux_losses=dict(disparity=["WeightedTotalVariation"],
                                            central_view=["NormalizedStructuralSimilarity", "NormalizedCosineProximity"]),
                            gradient_approximation="every_twentieth",
                            multi_task_uncertainty=True)


@network.named_config
def loss_cs_vector_dict():
    loss_disp = None
    loss_disp_kwargs = None
    loss_central = None
    loss_central_kwargs = None
    loss_weights = None
    loss_lf = "MeanSquaredError"
    loss_lf_kwargs = dict()


@network.named_config
def loss_cs_vector_dict_epinet():
    loss_disp = "MeanSquaredError"
    loss_disp_kwargs = dict()
    loss_central = "MeanSquaredError"
    loss_central_kwargs = dict()
    loss_weights = None


@network.named_config
def loss_cs_tensor_dict():
    loss_disp = None
    loss_disp_kwargs = None
    loss_central = None
    loss_central_kwargs = None
    loss_weights = None
    loss_lf = "MeanSquaredError"
    loss_lf_kwargs = dict()


@network.named_config
def loss_cs_tensor_dict_epinet():
    loss_disp = "MeanSquaredError"
    loss_disp_kwargs = dict()
    loss_central = "MeanSquaredError"
    loss_central_kwargs = dict()
    loss_weights = None


@network.named_config
def loss_mask_optimization():
    loss_disp = "Huber"
    loss_disp_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_central = "Huber"
    loss_central_kwargs = dict(delta=1.0, ver='lfcnn')
    loss_weights = dict(disparity=10.0, central_view=100.0)
