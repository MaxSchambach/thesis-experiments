# thesis-experiments

To recreate the results of the thesis, follow these steps.


## Setup MongoDB
The experiments are logged using [Sacred](https://github.com/IDSIA/sacred).
By default, Sacred loggs to a MongoDB to track all experiments.
Here, the helper library [mdbh](https://gitlab.com/MaxSchambach/mdbh) is used to connect and authenticate against the MongoDB instace.
See the corresponding [Wiki](https://gitlab.com/MaxSchambach/mdbh/-/wikis/mongodb-setup) for details.
Alternatively, you can change or disable the Sacred observer locally in ``config/base_config.py`` in the ``get_experiment`` function.


## Setup Python environment

Create a Python environment and install the required packages, in particular the `lfcnn` package which provides the reconsturction networks.
In particular, install via pip:

- [lfcnn](https://gitlab.com/iiit-public/lfcnn)
- [plenpy](https://gitlab.com/iiit-public/plenpy)
- [sacred](https://github.com/IDSIA/sacred)
- [tf_addons](https://www.tensorflow.org/addons/overview)


## Download the datasets

Download the [training, validation, and test dataset](https://ieee-dataport.org/open-access/multispectral-light-field-dataset-light-field-deep-learning).
For the evaluation, download the [real-world dataset](https://dx.doi.org/10.35097/500).

Setup your system paths to the dataset in ``config/dataset.py``


## Running an experiment

All experiments are named following the naming scheme of the thesis.

As an example, see the simple baseline experiment ``mt_naive.py`` using the 3D convolutional network a naive multi-task training strategy.
First, set the name of you MongoDB database if it deviates from the default.
If you want or need to update any parameters, such as the batch size, do so in the `CONFIG_UPDATES` dictionary as shown.
However, you can simply leave all parameters at their default.

For the training, you can specify all options available in ``config/settings.py``.
This includes:

  - `batch_size`: Size of the mini-batch for training. Default: 128
  - `epochs`: Number of epochs to train. Default: 170
  - `save_weights`: Whether to save the network weights. Default: `True`
  - `augment`: Specify which augmentations to use. Default:  Default: `None`
  - `use_mask`: Whether to use a spectral coding mask. Default: `True`
  - `mask_type`: Specify coding mask. Default: `random`
  - `psnr`: Whether to add noise to input light fields. Default: `None`
  - `data_percentage`: Percentage of dataset to use for training (useful for debugging.) Default: 1.0
  - `valid_percentage`: Percentage of validation dataset to use for validation. Default: 1.0
  - `verbose`: Enable or disable verbose output. Default: 0
  - `fix_seed_test`: Whether to fix the random seed for testing. Default: `True`
  - `fix_seed_eval`: Whether to fix the random seed for evaluation. Default: `True`
  - `shuffle`: Whether to shuffle the training data for each epoch. Default: `True`
  - `mixed_precision`: Whether to use mixed precision. Default: `False`
  - `validation_freq`: Validation frequency, that is validate only every x-th batch. Default: 1.0
  - `use_multiprocessing`: Whether to use multiprocessing for the data generation. Default: `True`
  - `workers`: How many processes to use. Default: 4
  - `max_queue_size`: Length of the data generation queue. Default: 4

You can further specify options for `dataset.py`, for example whether to temporarily copy the dataset to `$TMP` which is for example useful when performing the training on a shared workstation or cluster.

Run the experiment by calling

```bash
python <experiment>.py
```


## Setup trained models

When using pretrained models, e.g. for further evaluation or training, setup up the correct run IDs in ``config/trained_model.py``.
For example, this is necessary when learning a sparse dictionary and then evaluating the reconstruction and disparity estimation subsequently.