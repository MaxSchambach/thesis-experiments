"""
Learn fractal coding mask from scratch jointly with reconstruction.

Update the settings and network parameters below to obtain results for 5x5 and 6x6 fractal and regular masks.
"""

from config.base_config import get_experiment
from config.base_config import train_and_test, evaluate, predict, model_save_mask

# Set experiment, load default ingredients
ex = get_experiment("MaskFromScratchFractal4x4")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "network.model_conv3d_mask_fractal",
                 "network.loss_normalized_gradient_similarity_multi_task_uncertainty"]

CONFIG_UPDATES = {"dataset.copy": False,
                  # Turn of mask coding by generator. Coding is performed in network
                  "settings.use_mask": False,  
                  "settings.mask_type": None,
                  "network.mask_kwargs.pattern_shape": (4, 4),
                  "network.lambda_e_init": None,
                  "network.lambda_e_end": 1e3,
                  "network.inv_temp_init": None,
                  "network.inv_temp_end": 175.0,
                  "network.mask_kwargs.hard_forward": True,
                  "network.mask_kwargs.compress": "full",
                  "network.mask_kwargs.regular": False}

@ex.capture
def run_experiment(settings, network, _run, _log):
    model = train_and_test(settings, network, _run, _log)
    model_save_mask("coding_mask_32x32", model, _run, _log)

    evaluate(model, "challenges", settings, network, _run, _log)
    evaluate(model, "wall", settings, network, _run, _log)
    model_save_mask("coding_mask_512x512", model, _run, _log)

    predict(model, "own", settings, network, _run, _log)
    model_save_mask("coding_mask_400x400", model, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
