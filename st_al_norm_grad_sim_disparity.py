"""
ST AL NormGradSim experiment (disparity).
"""

from config.base_config import get_experiment
from config.base_config import train_and_test, evaluate, predict

# Set experiment, load default ingredients
ex = get_experiment("StAlNormGradSimDisparity")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "network.model_conv3d_st_disp",
                 "network.loss_disparity_single_norm_grad_sim"]

CONFIG_UPDATES = {"dataset.copy": True,
                  "settings.batch_size": 64}


@ex.capture
def run_experiment(settings, network, _run, _log):
    model = train_and_test(settings, network, _run, _log)
    evaluate(model, "challenges", settings, network, _run, _log)
    evaluate(model, "wall", settings, network, _run, _log)
    predict(model, "own", settings, network, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
