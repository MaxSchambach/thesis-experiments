"""
CS Tensor Dictionary learning.
"""

from config.base_config import get_experiment
from config.base_config import train_and_test

# Set experiment, load default ingredients
ex = get_experiment("CsTensorDictTrain")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "network.cs_tensor_dict_train",
                 "network.loss_cs_tensor_dict"]

CONFIG_UPDATES = {"dataset.copy": False,
                  "dataset.valid_path": None,
                  "settings.use_mask": False,  # For the dictionary training, do not use a coding mask
                  "settings.epochs": 1,
                  "settings.batch_size": 2}


@ex.capture
def run_experiment(settings, network, _run, _log):
    model = train_and_test(settings, network, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
