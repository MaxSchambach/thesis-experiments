"""
Mask training random
Mask predict random macropixel

Run experiment 10 times.
"""

from config.base_config import get_experiment
from config.base_config import get_trained_model
from config.base_config import evaluate
from config.base_config import predict
from config.base_config import test

# Set experiment, load default ingredients
ex = get_experiment("MaskTrainRandomMacroPxPredictRandomMacroPx")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "network.model_conv3d",
                 "trained_model.mt_uncertainty_al_norm_grad_sim"]

CONFIG_UPDATES = {"dataset.copy": False,
                  "settings.use_mask": True,
                  "settings.mask_type": "random_macropixel",
                  "settings.fix_seed_test": False,  # Use different seeds, run experiment 10 times
                  "settings.fix_seed_eval": False,
                  "settings.batch_size": 16}


@ex.capture
def run_experiment(settings, network, trained_model, _run, _log):

    model = get_trained_model(trained_model, network)
    test(model, settings, network, _run, _log)
    evaluate(model, "challenges", settings, network, _run, _log)
    evaluate(model, "wall", settings, network, _run, _log)
    predict(model, "own", settings, network, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
