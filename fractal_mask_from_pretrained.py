"""
Learn fractal coding mask from pretrained model.

Update the settings and network parameters below to obtain results for 5x5 and 6x6 fractal and regular masks.
"""

from config.base_config import get_experiment
from config.base_config import train_mask_optimization, evaluate, predict, get_trained_model, model_save_mask

# Set experiment, load default ingredients
ex = get_experiment("MaskFromPretrainedFractal4x4")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "settings.mask_optimization",
                 "network.model_conv3d_mask_fractal",
                 "trained_model.mt_uncertainty_al_norm_grad_sim",
                 "network.loss_mask_optimization"]

CONFIG_UPDATES = {"dataset.copy": True,
                  "settings.epochs_warmup": 5,
                  "settings.epochs": 20,
                  "settings.epochs_finetune": 10,
                  "settings.verbose": 1,
                  "network.mask_kwargs.pattern_shape": (4, 4),
                  "network.mask_kwargs.hard_forward": True,
                  "network.mask_kwargs.compress": "full",
                  "network.mask_kwargs.regular": False,
                  "settings.batch_size": 64}

@ex.capture
def run_experiment(settings, network, trained_model, dataset, _run, _log):
    model = get_trained_model(trained_model, network, predict=None)
    model = train_mask_optimization(model, settings, network, dataset, _run, _log)
    model_save_mask("coding_mask_32x32", model, _run, _log)

    evaluate(model, "challenges", settings, network, _run, _log)
    evaluate(model, "wall", settings, network, _run, _log)
    model_save_mask("coding_mask_512x512", model, _run, _log)

    predict(model, "own", settings, network, _run, _log)
    model_save_mask("coding_mask_400x400", model, _run, _log)
    predict(model, "xiong", settings, network, _run, _log)

@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
