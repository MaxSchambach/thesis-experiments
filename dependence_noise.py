"""
Evaluate the dependence on the inpout noise of the 3D convolutional network.
"""

from config.base_config import get_experiment
from config.base_config import get_trained_model
from config.base_config import test_noisy

# Set experiment, load default ingredients
ex = get_experiment("Conv3dPredictNoisy")
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 "network.model_conv3d",
                 "trained_model.conv3d"]

CONFIG_UPDATES = {"dataset.copy": False,
                  "settings.use_mask": True,
                  "settings.psnr": None}


@ex.capture
def run_experiment(settings, network, trained_model, _run, _log):

    model = get_trained_model(trained_model, network)
    test_noisy(model, settings, network, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
