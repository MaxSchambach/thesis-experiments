"""
Evaluate the dependence on the angular resolution of the 3D convolutional network.
"""

from config.base_config import get_experiment
from config.base_config import train_and_test, evaluate, predict

# Set experiment, load default ingredients
size = "7x7"

if size == "9x9":
    modelname = "network.model_conv3d"
    expname = "Conv3d9x9"

elif size == "7x7":
    modelname = "network.model_conv3d_7x7"
    expname = "Conv3d7x7"

elif size == "5x5":
    modelname = "network.model_conv3d_5x5"
    expname = "Conv3d5x5"

elif size == "3x3":
    modelname = "network.model_conv3d_3x3"
    expname = "Conv3d3x3"

else:
    raise ValueError()

ex = get_experiment(expname)
ex.add_source_file(__file__)

# Set experiment configuration
NAMED_CONFIGS = ["dataset.multispectral",
                 modelname,
                 "network.loss_normalized_gradient_similarity_multi_task_uncertainty"]

CONFIG_UPDATES = {"dataset.copy": False,
                  "settings.epochs": 1}

@ex.capture
def run_experiment(settings, network, _run, _log):
    model = train_and_test(settings, network, _run, _log)
    evaluate(model, "challenges", settings, network, _run, _log)
    evaluate(model, "wall", settings, network, _run, _log)
    predict(model, "own", settings, network, _run, _log)


@ex.main
def main():
    run_experiment()


if __name__ == '__main__':
    # Run with --force such that conflicting dictionary error in configuration
    # are ignored, e.g. for loss_kwargs
    ex.run(named_configs=NAMED_CONFIGS, config_updates=CONFIG_UPDATES,
           options={"--force": True})
